$(document).ready(function() {
  'use strict';
  // VÙNG 1: Biến toàn cục 
  var gDatabase = []; // biến lưu all dữ liệu
  var gSelectedId = 0; // biến lưu id dữ liệu đã chọn
  var gTable = $("#album-table").DataTable({
    "columns": [
      {"data":"albumCode"},
      {"data":"albumName"},
      {"data":"description"},
      {"data":"createDate"},
      {"data":"updateDate"},
    ]
  })

  // VÙNG 2: gán sự kiện cho các phần tử
  $(document).ready(function() { // hàm chạy khi load trang
    callAjaxApiGetAllAlbum();
  })
  gTable.on("click", "td", function() { // gán sự kiện click vào 1 row trong table, load dữ liệu vào form 
    onTableRowClick(this);
  });
  $("#btn-add-data").on("click", onBtnAddDataClick) // gán sự kiện click vào nút thêm mới dữ liệu 
  $("#btn-update-data").on("click", onBtnUpdateDataClick) // gán sự kiện click vào nút cập nhật dữ liệu
  $("#btn-delete-data").on("click", onBtnDeleteDataClick) // gán sự kiện click vào nút xóa dữ liệu

  // VÙNG 3: hàm xử lý sự kiện
  function onTableRowClick(paramTableCell) { // xử lý sự kiện khi click vào 1 row trong table, load dữ liệu vào form
    "use strict";
    var vClickedRow = $(paramTableCell).parents("tr");
    var vRowData = gTable.row(vClickedRow).data();
    gSelectedId = vRowData.id;			
    console.log("Đã click vào row có id là: " + gSelectedId);
    calApiGetAlbumById(gSelectedId);
  }
  function onBtnAddDataClick() {
    gSelectedId = 0; // gán Id đã chọn là 0 nếu đang muốn add dữ liệu
    // B1: đọc dữ liệu từ form
    var vAlbumData = thuThapDuLieuAlbum();
    // B2: validate dữ liệu
    var vIsValid = kiemTraAlbumData(vAlbumData);
    if (vIsValid == true) {
      // B3: gọi api
      callApiAddNewAlbum(vAlbumData);
    }
  }
  function onBtnUpdateDataClick() { // xử lý sự kiện khi click vào nút cập nhật dữ liệu
    if(gSelectedId == 0) {
      alert("Chưa chọn Album nào");
    }
    else {
      // B1: đọc dữ liệu từ form
      var vAlbumData = thuThapDuLieuAlbum();
      // B2: validate dữ liệu
      var vIsValid = kiemTraAlbumData(vAlbumData);
      if(vIsValid == true) {
        // B3: gọi api
        callApiUpdateAlbumById(gSelectedId, vAlbumData);
      }
    }
  }
  function onBtnDeleteDataClick() {
    if(gSelectedId == 0) {
      alert("Chưa chọn Album nào");
    }
    else {
      var vConfirmDelete = confirm("Xóa Album có id " + gSelectedId + "?");
      if(vConfirmDelete == true) {
        callApiDeleteAlbumById(gSelectedId);
      }
    }
  }

  // VÙNG 4: hàm dùng chung
  function callAjaxApiGetAllAlbum() { // gọi api lấy list all dữ liệu
    $.ajax({
      url: "http://localhost:8080/albums",
      type: "GET",
      async: false,
      dataType: "json",
      success: function(res) {
        console.log(res);
        gDatabase = res;
        loadAlbumToTable(gDatabase);
      },
      error: function(err) {
        console.log(err.response);
      }
    })
  }
  function loadAlbumToTable(paramListAlbum) { // đổ list dữ liệu vào table
    "use strict";
    gTable.clear();
    gTable.rows.add(paramListAlbum);
    gTable.draw();
  }
  function calApiGetAlbumById(paramId) { // gọi api lấy dữ liệu theo id
    "use strict";
    $.ajax({
      url: "http://localhost:8080/albums/"  + paramId,
      type: "GET",
      dataType: "json",
      success: function(res) {
        console.log(res);
        loadAlbumDataToForm(res);
      },
      error: function(err){
        console.log(err.response);
      }
    })
  }
  function loadAlbumDataToForm(paramAlbum) { // đổ dữ liệu vào form
    "use strict";
    $("#inp-ma-album").val(paramAlbum.albumCode);
    $("#inp-ten-album").val(paramAlbum.albumName);
    $("#inp-mo-ta-album").val(paramAlbum.description);
    $("#inp-ngay-tao").val(paramAlbum.createDate);
    $("#inp-ngay-cap-nhat").val(paramAlbum.updateDate);
  }
  function thuThapDuLieuAlbum() { // đọc dữ liệu từ form
    var vAlbumData = {
      albumCode: $.trim($("#inp-ma-album").val()),
      albumName: $.trim($("#inp-ten-album").val()),
      description: $.trim($("#inp-mo-ta-album").val()),
      createDate: $.trim($("#inp-ngay-tao").val()),
      updateDate: $.trim($("#inp-ngay-cap-nhat").val())
    };			
    console.log("Thông tin Album trên form thu được là:");
    console.log(vAlbumData);
    return vAlbumData;
  }
  function kiemTraAlbumData(paramAlbum) { // kiểm tra dữ liệu từ form
    "use strict";
    if (paramAlbum.albumCode == "") {
      alert("Chưa nhập tên Mã Album")
    }
    if (checkMaAlbum(paramAlbum.albumCode, gSelectedId)) {
      alert("Mã Album này đã tồn tại");
    }
  
    if (paramAlbum.albumName == "") {
      alert("Chưa nhập tên Album")
    }
    if (paramAlbum.description == "") {
      alert("Chưa mô tả Album")
    }
   
    return true;
  }

  function checkMaAlbum(paramAlbum, paramId) {
    "use strict";
    // nếu paramId = 0, hàm kiểm tra để add dữ liệu, nếu paramId khác 0, hàm kiểm tra dể cập nhật dữ liệu
    var vIsExisted = false;
    var bI = 0;
    while (vIsExisted == false && bI < gDatabase.length) {
      if (gDatabase[bI].albumCode == paramAlbum && gDatabase[bI].id != paramId) {
        // nếu trùng mã của item nhưng id cũng bị trùng chứng tỏ đang cập nhật dữ liệu, nên ko xem là bị trùng mã
        // phải trùng mã của item và khác id thì chứng tỏ cập nhật dữ liệu bị trùng mã với item khác,
        // hoặc đang add dữ liệu (vì khi add dữ liệu, id luôn là 0 nên luôn khác id của item)
        vIsExisted = true
      }
      else {
        bI ++
      }
    }
    return vIsExisted
  }
 
  function callApiAddNewAlbum(paramAlbumData) { // gọi api thêm dữ liệu
    "use strict";
    $.ajax({
      url: "http://localhost:8080/albums/",
      type: "POST",
      contentType: "application/json; charset=UTF-8",
      data: JSON.stringify(paramAlbumData),
      success: function(res) {
        console.log("Tạo album mới thành công. Response là:");
        console.log(res);
        callAjaxApiGetAllAlbum();
      },
      error: function(err){
        console.log(err.response);
      }
    })
  }
  function callApiUpdateAlbumById(paramId, paramAlbumData) { // gọi api cập nhật dữ liệu theo id
    "use strict";
    $.ajax({
      url: "http://localhost:8080/albums/" + paramId,
      type: "PUT",
      contentType: "application/json; charset=UTF-8",
      data: JSON.stringify(paramAlbumData),
      success: function(res) {
        callAjaxApiGetAllAlbum();
        console.log("Update thành công cho Album có id " + paramId + ". Response là:");
        console.log(res);
        setTimeout(function(){
          alert("Update thành công cho Album có id " + paramId)}, 1000);
      },
      error: function(err){
        console.log(err.response);
        alert("Update không thành công. Xem console");
      }
    })
  }
  function callApiDeleteAlbumById(paramAlbumId) { // gọi api xóa dữ liệu theo id
    "use strict";
    $.ajax({
      url: "http://localhost:8080/albums/"  + paramAlbumId,
      type: "DELETE",
      dataType: "json",
      success: function(res) {
        gSelectedId = 0;
        callAjaxApiGetAllAlbum();
        console.log("Xóa Album có id " + paramAlbumId + " thành công. Response là:");
        console.log(res);
        setTimeout(function(){
          alert("Xóa Album có id " + paramAlbumId + " thành công!")}, 1000);
      },
      error: function(err){
        console.log(err.response);
        alert("Xóa Album có id " + paramAlbumId + " không thành công!");
      }
    })
  }
})
