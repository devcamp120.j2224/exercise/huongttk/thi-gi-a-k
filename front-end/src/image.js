$(document).ready(function() {
  'use strict';
  // VÙNG 1: Biến toàn cục 
  var gDatabase = []; // biến lưu all dữ liệu
  var gSelectedId = 0; // biến lưu id dữ liệu đã chọn
  var gTable = $("#image-table").DataTable({
    "columns": [
      {"data":"imageCode"},
      {"data":"imageName"},
      {"data":"description"},
      {"data":"link"},
      {"data":"albumId"},
    ]
  })

  // VÙNG 2: gán sự kiện cho các phần tử
  $(document).ready(function() { // hàm chạy khi load trang
    callAjaxGetAllImage();
   callAjaxGetAllAlbum();
  })
  gTable.on("click", "td", function() { // gán sự kiện click vào 1 row trong table, load dữ liệu vào form 
    onTableRowClick(this);
  });
  $("#btn-add-data").on("click", onBtnAddDataClick) // gán sự kiện click vào nút thêm mới dữ liệu 
  $("#btn-update-data").on("click", onBtnUpdateDataClick) // gán sự kiện click vào nút cập nhật dữ liệu
  $("#btn-delete-data").on("click", onBtnDeleteDataClick) // gán sự kiện click vào nút xóa dữ liệu

  // VÙNG 3: hàm xử lý sự kiện
  function onTableRowClick(paramTableCell) { // xử lý sự kiện khi click vào 1 row trong table, load dữ liệu vào form
    "use strict";
    var vClickedRow = $(paramTableCell).parents("tr");
    var vRowData = gTable.row(vClickedRow).data();
    gSelectedId = vRowData.id;			
    console.log("Đã click vào row có id là: " + gSelectedId);
    callAjaxGetImageById(gSelectedId);
  }
  function onBtnAddDataClick() {
    gSelectedId = 0; // gán Id đã chọn là 0 nếu đang muốn add dữ liệu
    // B1: đọc dữ liệu từ form
    var vImageData = readImageDataFromWeb();
    // B2: validate dữ liệu
    var vIsValid = validateImageData(vImageData);
    if (vIsValid == true) {
      // B3: gọi api
      callAjaxAddNewImage(vImageData);
    }
  }
  function onBtnUpdateDataClick() { // xử lý sự kiện khi click vào nút cập nhật dữ liệu
    if(gSelectedId == 0) {
      alert("Chưa chọn Image nào");
    }
    else {
      // B1: đọc dữ liệu từ form
      var vImageData = readImageDataFromWeb();
      // B2: validate dữ liệu
      var vIsValid = validateImageData(vImageData);
      if(vIsValid == true) {
        // B3: gọi api
        callAjaxUpdateImage(gSelectedId, vImageData);
      }
    }
  }
  function onBtnDeleteDataClick() {
    if(gSelectedId == 0) {
      alert("Chưa chọn Image nào");
    }
    else {
      var vConfirmDelete = confirm("Xóa Image có id " + gSelectedId + "?");
      if(vConfirmDelete == true) {
        callAjaxApiDeleteImageById(gSelectedId);
      }
    }
  }

  // VÙNG 4: hàm dùng chung
  function callAjaxGetAllImage() { // gọi api lấy list all dữ liệu
    $.ajax({
      url: "http://localhost:8080/images",
      type: "GET",
      async: false,
      dataType: "json",
      success: function(res) {
        console.log(res);
        gDatabase = res;
        loadListImageToTable(gDatabase);
      },
      error: function(err) {
        console.log(err.response);
      }
    })
  }
  function loadListImageToTable(paramImage) { // đổ list dữ liệu vào table
    "use strict";
    gTable.clear();
    gTable.rows.add(paramImage);
    gTable.draw();
  }
  function callAjaxGetAllAlbum() { // gọi api lấy list all dữ liệu PhongBan
    $.ajax({
      url: "http://localhost:8080/albums",
      type: "GET",
      async: false,
      dataType: "json",
      success: function(res) {
        console.log(res);
        loadListAlbumToSelect(res);
      },
      error: function(err) {
        console.log(err.response);
      }
    })
  }
  function loadListAlbumToSelect(paramImage) {
    for (let bAlbum of paramImage) {
      $("#select-album").append($("<option>").val(bAlbum.id).html(bAlbum.albumName));
    };
  }
  function callAjaxGetImageById(paramId) { // gọi api lấy dữ liệu theo id
    "use strict";
    $.ajax({
      url: "http://localhost:8080/images/"  + paramId,
      type: "GET",
      dataType: "json",
      success: function(res) {
        console.log(res);
        loadImageDataToForm(res);
      },
      error: function(err){
        console.log(err.response);
      }
    })
  }
  function loadImageDataToForm(paramImage) { // đổ dữ liệu vào form
    "use strict";
    $("#inp-ma-image").val(paramImage.imageCode);
    $("#inp-name-image").val(paramImage.imageName);
    $("#inp-mo-ta").val(paramImage.description);
    $("#inp-link").val(paramImage.link);
    $("#select-album").val(paramImage.albumId);
  }
  function readImageDataFromWeb() { // đọc dữ liệu từ form
    var vImageData = {
      imageCode: $.trim($("#inp-ma-image").val()),
      imageName: $.trim($("#inp-name-image").val()),
      description: $.trim($("#inp-mo-ta").val()),
      link: $.trim($("#inp-link").val()),
      albumId: $.trim($("#select-album").val())  
    };			
    console.log("Thông tin Image đọc được là:");
    console.log(vImageData);
    return vImageData;
  }
  function validateImageData(paramImage) { // kiểm tra dữ liệu từ form
    "use strict";
    if (paramImage.imageCode == "") {
      alert("Chưa nhập Mã Image")
    }
    if (checkMaImageDaTonTai(paramImage.imageCode, gSelectedId) == true) {
      alert("Mã Image này đã tồn tại");
    }
    if (paramImage.imageName == "") {
      alert("Chưa nhập tên Image")
    }
    if (paramImage.link == "") {
      alert("Chưa nhập Link Image")
    }
    if (paramImage.description == "") {
      alert("Chưa mô tả về Image")
    }
    if (paramImage.albumId == "") {
      alert("Chưa chọn Album")
    }
    return true;
  }

  function checkMaImageDaTonTai(paramImage, paramId) {
    "use strict";
    // nếu paramId = 0, hàm kiểm tra để add dữ liệu, nếu paramId khác 0, hàm kiểm tra dể cập nhật dữ liệu
    var vIsExisted = false;
    var bI = 0;
    while (vIsExisted == false && bI < gDatabase.length) {
      if (gDatabase[bI].imageName == paramImage && gDatabase[bI].id != paramId) {
        // nếu trùng mã của item nhưng id cũng bị trùng chứng tỏ đang cập nhật dữ liệu, nên ko xem là bị trùng mã
        // phải trùng mã của item và khác id thì chứng tỏ cập nhật dữ liệu bị trùng mã với item khác,
        // hoặc đang add dữ liệu (vì khi add dữ liệu, id luôn là 0 nên luôn khác id của item)
        vIsExisted = true
      }
      else {
        bI ++
      }
    }
    return vIsExisted
  }
 
  function callAjaxAddNewImage(paramImage) { // gọi api thêm dữ liệu
    "use strict";
    $.ajax({
      url: "http://localhost:8080/images?albumId=" + paramImage.albumId,
      type: "POST",
      contentType: "application/json; charset=UTF-8",
      data: JSON.stringify(paramImage),
      success: function(res) {
        console.log("Tạo image mới thành công. Response là:");
        console.log(res);
        callAjaxGetAllImage();
       
      },
      error: function(err){
        console.log(err.response);
      }
    })
  }
  function callAjaxUpdateImage(paramId, paramImage) { // gọi api cập nhật dữ liệu theo id
    "use strict";
    $.ajax({
      url: "http://localhost:8080/images/"  + paramId,
      type: "PUT",
      contentType: "application/json; charset=UTF-8",
      data: JSON.stringify(paramImage),
      success: function(res) {
        callAjaxGetAllImage();
        console.log("Update thành công cho Image có id " + paramId + ". Response là:");
        console.log(res);
        setTimeout(function(){
          alert("Update thành công cho Image có id " + paramId)}, 1000);
      },
      error: function(err){
        console.log(err.response);
        alert("Update không thành công. Xem console");
      }
    })
  }
  function callAjaxApiDeleteImageById(paramImageId) { // gọi api xóa dữ liệu theo id
    "use strict";
    $.ajax({
      url: "http://localhost:8080/images/"  + paramImageId,
      type: "DELETE",
      dataType: "json",
      success: function(res) {
        gSelectedId = 0;
        callAjaxGetAllImage();
        console.log("Xóa Image có id " + paramImageId + " thành công. Response là:");
        console.log(res);
        setTimeout(function(){
          alert("Xóa Image có id " + paramImageId + " thành công!")}, 1000);
      },
      error: function(err){
        console.log(err.response);
        alert("Xóa Image có id " + paramImageId + " không thành công!");
      }
    })
  }
})
