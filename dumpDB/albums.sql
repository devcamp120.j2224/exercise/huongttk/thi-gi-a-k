-- phpMyAdmin SQL Dump
-- version 5.2.0
-- https://www.phpmyadmin.net/
--
-- Máy chủ: 127.0.0.1
-- Thời gian đã tạo: Th8 16, 2022 lúc 05:05 AM
-- Phiên bản máy phục vụ: 10.4.24-MariaDB
-- Phiên bản PHP: 7.4.29

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Cơ sở dữ liệu: `exam_album`
--

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `albums`
--

CREATE TABLE `albums` (
  `id` bigint(20) NOT NULL,
  `album_code` varchar(8) DEFAULT NULL,
  `album_name` varchar(255) NOT NULL,
  `create_date` datetime DEFAULT NULL,
  `description` varchar(255) DEFAULT NULL,
  `update_date` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Đang đổ dữ liệu cho bảng `albums`
--

INSERT INTO `albums` (`id`, `album_code`, `album_name`, `create_date`, `description`, `update_date`) VALUES
(1, 'AL123', 'Ánh nắng', NULL, 'nắng chiều', NULL),
(2, 'AL456', 'Tuyết', NULL, 'bông tuyết trắng tinh', '2022-08-16 06:41:01'),
(4, 'AL987', 'cánh đồng', '2022-08-16 06:39:37', 'lúa vàng trên cánh đồng', NULL),
(5, 'AL951', 'Mùa xuân', '2022-08-16 09:40:14', 'cảnh sắc hoa mai, hoa đào', NULL),
(7, 'AL621', 'Mùa hạ', '2022-08-16 09:41:49', 'trời trong xanh', NULL),
(8, 'AL632', 'Mùa Thu', '2022-08-16 09:42:55', 'lá rơi', NULL),
(9, 'AL845', 'Mùa Đông', '2022-08-16 09:43:37', 'tuyết rơi', NULL),
(10, 'AL981', 'Sông núi', '2022-08-16 09:47:35', 'thiên nhiên núi rừng', NULL),
(11, 'AL462', 'Đại dương', '2022-08-16 09:50:16', 'biển xanh, cát vàng', NULL),
(13, 'AL532', 'Lễ hội', '2022-08-16 09:52:17', 'quang cảnh nhộn nhịp', NULL);

--
-- Chỉ mục cho các bảng đã đổ
--

--
-- Chỉ mục cho bảng `albums`
--
ALTER TABLE `albums`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `UK_h7tyk8kn08kwun4pk76r2dy11` (`album_code`);

--
-- AUTO_INCREMENT cho các bảng đã đổ
--

--
-- AUTO_INCREMENT cho bảng `albums`
--
ALTER TABLE `albums`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
