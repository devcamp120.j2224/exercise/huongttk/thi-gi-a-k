-- phpMyAdmin SQL Dump
-- version 5.2.0
-- https://www.phpmyadmin.net/
--
-- Máy chủ: 127.0.0.1
-- Thời gian đã tạo: Th8 16, 2022 lúc 05:05 AM
-- Phiên bản máy phục vụ: 10.4.24-MariaDB
-- Phiên bản PHP: 7.4.29

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Cơ sở dữ liệu: `exam_album`
--

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `images`
--

CREATE TABLE `images` (
  `id` bigint(20) NOT NULL,
  `description` varchar(255) DEFAULT NULL,
  `image_code` varchar(255) DEFAULT NULL,
  `image_name` varchar(255) NOT NULL,
  `link` varchar(255) NOT NULL,
  `album_id` bigint(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Đang đổ dữ liệu cho bảng `images`
--

INSERT INTO `images` (`id`, `description`, `image_code`, `image_name`, `link`, `album_id`) VALUES
(2, 'màu sắc tươi sáng', 'IM14', 'trăm hoa khoe sắc', 'http://muaxuan/hoakhoesac', 2),
(4, 'màu sắc tươi sáng', 'IM142', 'trăm hoa khoe sắc', 'http://anhnang/hoakhoesac', 1),
(5, 'màu sắc tươi sáng', 'IM258', 'bông tuyết rơi', 'http://tuyet/bongtuyet', 2),
(6, 'màu sắc rực rỡ', 'IM654', 'lúa vàng', 'http://canhdong/lúa vàng', 4),
(7, 'Đám trẻ nô đùa', 'IM284', 'Cánh diều bay xa', 'http://muaha/canhdieu', 7),
(8, 'Đám trẻ nô đùa', 'IM619', 'Lễ hội dân gian', 'http://lehoi/dangian', 13),
(9, 'cùng vượt sóng', 'IM785', 'lướt sóng', 'http://daiduong/luotsong', 11),
(10, 'rừng lá phong được chuyển màu đỏ', 'IM785', 'Mùa lá đỏ', 'http://muathu/lado', 8),
(11, 'ngày xuân khoe sắc', 'IM785', 'hoa tết', 'http://muaxuan/hoatet', 5);

--
-- Chỉ mục cho các bảng đã đổ
--

--
-- Chỉ mục cho bảng `images`
--
ALTER TABLE `images`
  ADD PRIMARY KEY (`id`),
  ADD KEY `FK724cv7ds8vwsp7mpi6e5s7keq` (`album_id`);

--
-- AUTO_INCREMENT cho các bảng đã đổ
--

--
-- AUTO_INCREMENT cho bảng `images`
--
ALTER TABLE `images`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- Các ràng buộc cho các bảng đã đổ
--

--
-- Các ràng buộc cho bảng `images`
--
ALTER TABLE `images`
  ADD CONSTRAINT `FK724cv7ds8vwsp7mpi6e5s7keq` FOREIGN KEY (`album_id`) REFERENCES `albums` (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
