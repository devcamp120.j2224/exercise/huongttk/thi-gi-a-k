package com.exam.j0405album.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import com.exam.j0405album.models.Album;
import com.exam.j0405album.models.Image;
import com.exam.j0405album.repository.IAlbumRepository;
import com.exam.j0405album.repository.IImageRepository;

@Service
public class ImageService {
   
    //-------------IMPOTR INTERFACE--------------
    @Autowired
   IImageRepository imageRepository;
   @Autowired
   IAlbumRepository albumRepository;

    //-------------GET---------------------

    public ResponseEntity <List<Image>> getAllImage() {
        try {
            List <Image> listImage = new ArrayList<Image>();
            imageRepository.findAll().forEach(listImage :: add);
            return new ResponseEntity<>(listImage, HttpStatus.OK);
        } catch (Exception ex) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    public ResponseEntity <Object> getImageById(Long id) {
        Optional<Image> imageData = imageRepository.findById(id);
        if (imageData.isPresent()) {
            return new ResponseEntity<>(imageData.get(), HttpStatus.OK);
        } else {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

    // GET IMAGE BY ALBUMID
	public ResponseEntity<Object> getAllImageByAlbumId(Long id) {
		Optional<Album> albumData = albumRepository.findById(id);
		if (albumData.isPresent())
			return new ResponseEntity<>(albumData.get().getImages(), HttpStatus.OK);
		else
			return new ResponseEntity<>("Album not found", HttpStatus.NOT_FOUND);
	}


    //-------------------------POST-----------------------

    public ResponseEntity<Object> createImage(Long albumId, Image image) {
        Optional<Album> albumData = albumRepository.findById(albumId);

		if (albumData.isPresent()) {
			try {
                Image newImage = new Image();
                newImage.setImageCode(image.getImageCode());
                newImage.setImageName(image.getImageName());
                newImage.setDescription(image.getDescription());
                newImage.setLink(image.getLink());
				newImage.setAlbum(albumData.get());
				return new ResponseEntity<>(imageRepository.save(newImage), HttpStatus.CREATED);
			} catch (Exception e) {
				System.out.println("+++++++++++++++++++++::::: " + e.getCause().getCause().getMessage());
				return ResponseEntity.unprocessableEntity()
					.body("Failed to Create specified Image: " + e.getCause().getCause().getMessage());
			}
		} else {
			return new ResponseEntity<>("Image not found", HttpStatus.NOT_FOUND);
		}

    }

    //------------- PUT--------------------

    public ResponseEntity<Object> updateImageById(Long id, Image image) {
        Optional<Image> imageData = imageRepository.findById(id);
      
		if (imageData.isPresent()) {
				try {
                    Image imageUpdate = imageData.get();
                    imageUpdate.setImageCode(image.getImageCode());
                    imageUpdate.setImageName(image.getImageName());
                    imageUpdate.setDescription(image.getDescription());
                    imageUpdate.setLink(image.getLink());
				return new ResponseEntity<>(imageRepository.save(imageUpdate), HttpStatus.OK);
			} catch (Exception e) {
				System.out.println("+++++++++++++++++++++::::: " + e.getCause().getCause().getMessage());
				return ResponseEntity.unprocessableEntity()
					.body("Failed to Update specified Image: " + e.getCause().getCause().getMessage());
			}
		} else {
			return new ResponseEntity<>("Image not found", HttpStatus.NOT_FOUND);
		}
    }

    //////////////////////////// DELETE SERVICES///////////////////////////
    public ResponseEntity<Object> deleteImageById(Long id) {
        try {
            imageRepository.deleteById(id);
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        } catch (Exception e) {
            System.out.println(e);
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    public ResponseEntity<Object> deleteAllImage() {
        try {
            imageRepository.deleteAll();
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        } catch (Exception e) {
            System.out.println(e);
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
}
