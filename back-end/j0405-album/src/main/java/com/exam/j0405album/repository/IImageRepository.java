package com.exam.j0405album.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.exam.j0405album.models.Image;
@Repository
public interface IImageRepository extends JpaRepository <Image, Long>{
   
}
