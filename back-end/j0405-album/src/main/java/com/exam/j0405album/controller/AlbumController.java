package com.exam.j0405album.controller;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.exam.j0405album.models.Album;
import com.exam.j0405album.service.AlbumService;

@RestController
@CrossOrigin
@RequestMapping("/albums")
public class AlbumController {
    @Autowired
    private AlbumService albumService;

    //--------------------GET--------------
    @GetMapping
    public ResponseEntity<List<Album>> getListAlbum(){
        ResponseEntity<List<Album>> respon = albumService.getAllAlbum();
        return respon;
    }
    @GetMapping("{id}")
    public ResponseEntity <Object> getAlbumById(@PathVariable("id") long id) {
        ResponseEntity <Object> respon = albumService.getAlbumById(id);
        return respon;
    }


    //--------------POST------------
    @PostMapping
    public ResponseEntity <Object> postAlbum(@Valid @RequestBody Album album) {
        ResponseEntity <Object> responUser = albumService.createAlbum(album);
        return responUser;
    }


    //--------------PUT---------------
    @PutMapping("{id}")
    public ResponseEntity <Object> putAlbum(@PathVariable long id, @Valid @RequestBody Album album) {
        ResponseEntity <Object> respon = albumService.updateAlbumById(id, album);
        return respon;
    }

    //--------------DELETE-----------------
   
    @DeleteMapping("{id}")
    public ResponseEntity<Object> deleteAlbumById(@PathVariable Long id) {
        ResponseEntity<Object> responseUsers = albumService.deleteAlbumById(id);
        return responseUsers;
    }
    
    @DeleteMapping()
    public ResponseEntity<Object> deleteAllAlbum() {
        ResponseEntity<Object> responseUsers = albumService.deleteAllAlbum();
        return responseUsers;
    }
}
