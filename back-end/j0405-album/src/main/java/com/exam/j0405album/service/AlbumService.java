package com.exam.j0405album.service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import com.exam.j0405album.models.Album;
import com.exam.j0405album.repository.IAlbumRepository;

@Service
public class AlbumService {
    @Autowired
    //-------------IMPOTR INTERFACE--------------
    IAlbumRepository albumRepository;


    //-------------GET---------------------

    public ResponseEntity <List<Album>> getAllAlbum() {
        try {
            List <Album> listAlbum = new ArrayList<Album>();
            albumRepository.findAll().forEach(listAlbum :: add);
            return new ResponseEntity<>(listAlbum, HttpStatus.OK);
        } catch (Exception ex) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    public ResponseEntity <Object> getAlbumById(Long id) {
        Optional<Album> albumData = albumRepository.findById(id);
        if (albumData.isPresent()) {
            return new ResponseEntity<>(albumData.get(), HttpStatus.OK);
        } else {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

    //-------------------------POST-----------------------

    public ResponseEntity<Object> createAlbum(Album album) {
        try {
			Album newAlbum = new Album();
			newAlbum.setAlbumCode(album.getAlbumCode());
            newAlbum.setAlbumName(album.getAlbumName());
			newAlbum.setDescription(album.getDescription());
			newAlbum.setCreateDate(new Date());
			return new ResponseEntity<>(albumRepository.save(newAlbum), HttpStatus.CREATED);
		} catch (Exception e) {
			System.out.println("+++++++++++++++++++++::::: " + e.getCause().getCause().getMessage());
			return ResponseEntity.unprocessableEntity()
				.body("Failed to Create specified Album: " + e.getCause().getCause().getMessage());
		}
    }

    //------------- PUT--------------------

    public ResponseEntity<Object> updateAlbumById(Long id, Album album) {
        try {
            Optional<Album> albumData = albumRepository.findById(id);

            if (albumData.isPresent()) {
                Album albumUpdate = albumData.get();
                albumUpdate.setAlbumCode(album.getAlbumCode());
                albumUpdate.setAlbumName(album.getAlbumName());
                albumUpdate.setDescription(album.getDescription());
                albumUpdate.setImages(album.getImages());
                albumUpdate.setUpdateDate(new Date());
                return new ResponseEntity<>(albumRepository.save(albumUpdate), HttpStatus.OK);
            } else {
                return new ResponseEntity<>(HttpStatus.NOT_FOUND);
            }
        } catch (Exception e) {
            System.out.println(e);
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }

    }

    //////////////////////////// DELETE SERVICES///////////////////////////
    public ResponseEntity<Object> deleteAlbumById(Long id) {
        try {
            albumRepository.deleteById(id);
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        } catch (Exception e) {
            System.out.println(e);
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    public ResponseEntity<Object> deleteAllAlbum() {
        try {
            albumRepository.deleteAll();
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        } catch (Exception e) {
            System.out.println(e);
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
}
