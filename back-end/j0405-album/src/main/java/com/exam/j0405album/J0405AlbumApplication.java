package com.exam.j0405album;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class J0405AlbumApplication {

	public static void main(String[] args) {
		SpringApplication.run(J0405AlbumApplication.class, args);
	}

}
