package com.exam.j0405album.models;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotEmpty;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
@Table(name = "images")
public class Image {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @NotEmpty
    @Column(name = "image_code", unique = true)
    private String imageCode;

    @NotEmpty
    @Column(name = "image_name")
    private String imageName;

    @NotEmpty
    @Column(name = "description")
    private String description;

    @NotEmpty
    @Column(name = "link")
    private String link;

    @ManyToOne
    @JsonIgnore
    @JoinColumn(name = "album_id", nullable = false)
    private Album album;

    public Image() {
    }

    
    public Image(long id, @NotEmpty String imageCode, @NotEmpty String imageName, @NotEmpty String description,
            @NotEmpty String link, Album album) {
        this.id = id;
        this.imageCode = imageCode;
        this.imageName = imageName;
        this.description = description;
        this.link = link;
        this.album = album;
    }


    public long getId() {
        return id;
    }


    public void setId(long id) {
        this.id = id;
    }


    public String getImageCode() {
        return imageCode;
    }


    public void setImageCode(String imageCode) {
        this.imageCode = imageCode;
    }


    public String getImageName() {
        return imageName;
    }


    public void setImageName(String imageName) {
        this.imageName = imageName;
    }


    public String getDescription() {
        return description;
    }


    public void setDescription(String description) {
        this.description = description;
    }


    public String getLink() {
        return link;
    }


    public void setLink(String link) {
        this.link = link;
    }


    public Album getAlbum() {
        return album;
    }


    public void setAlbum(Album album) {
        this.album = album;
    }

    public Long getAlbumId() {
        return album.getId();
    }



}
