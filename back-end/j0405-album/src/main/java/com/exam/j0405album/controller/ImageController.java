package com.exam.j0405album.controller;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.exam.j0405album.models.Image;
import com.exam.j0405album.service.ImageService;

@RestController
@CrossOrigin
@RequestMapping("/images")
public class ImageController {
    @Autowired
    private ImageService imageService;

    //--------------------GET--------------
    @GetMapping
    public ResponseEntity<List<Image>> getAllImage(){
        ResponseEntity<List<Image>> respon = imageService.getAllImage();
        return respon;
    }
    @GetMapping("{id}")
    public ResponseEntity <Object> getImageById(@PathVariable("id") Long id) {
        ResponseEntity <Object> respon = imageService.getImageById(id);
        return respon;
    }


    // GET IMAGE BY ALBUMID
	@GetMapping("/albums/{id}")
	public ResponseEntity<Object> getImageByAlbumId(@PathVariable Long id) {
        ResponseEntity <Object> respon = imageService.getAllImageByAlbumId(id);
        return respon;
    }
   

    //--------------POST------------
    @PostMapping
    public ResponseEntity <Object> postImage(@RequestParam Long albumId, @Valid @RequestBody Image image) {
        ResponseEntity <Object> respon = imageService.createImage(albumId, image);
        return respon;
    }


    //--------------PUT---------------
    @PutMapping("{id}")
    public ResponseEntity <Object> putImage(@PathVariable Long id, @Valid @RequestBody Image image) {
        ResponseEntity <Object> respon = imageService.updateImageById(id, image);
        return respon;
    }

    //--------------DELETE-----------------
   
    @DeleteMapping("{id}")
    public ResponseEntity<Object> deleteImageById(@PathVariable Long id) {
        ResponseEntity<Object> respon = imageService.deleteImageById(id);
        return respon;
    }
    
    @DeleteMapping()
    public ResponseEntity<Object> deleteAllImage() {
        ResponseEntity<Object> respon = imageService.deleteAllImage();
        return respon;
    }
}
